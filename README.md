# Citlali

### Programa para la gestión de inventarió y punto de venta.

Escrito en python. Usa pyGObject (GTK3), [python-catrina](https://gitlab.com/wormius/python-catrina).

#### Capturas

Pantalla de inicio y selección de sucursal

![Pantalla de inicio y selección de sucursal](https://gitlab.com/wormius/citlali/-/raw/master/readmesrc/inicio.png)

Panel principal

![Panel principal](https://gitlab.com/wormius/citlali/-/raw/master/readmesrc/principal.png)

Añadir producto

![Pantalla para añadir producto](https://gitlab.com/wormius/citlali/-/raw/master/readmesrc/stock.png)

Seguridad

![Seguridad](https://gitlab.com/wormius/citlali/-/raw/master/readmesrc/seguridad.png)


##### Documentación pendiente...
