# -*- coding: utf-8 -*

import gi

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk

import os

import models

class Splash(object):

    def __init__(self):
        self.builder = Gtk.Builder()
        self.load()
        self.window = self.builder.get_object("citlali_splash_window")
        self.add_branch_window = self.builder.get_object("add_branch_window")
        self.add_branch_filechooser = self.builder.get_object("add_branch_window")

        self.branch_chooser = self.builder.get_object("select_branch_filechooser")
        last_branch_selected = models.Settings(models.INTERNALDB).get("last_branch_selected")

        self.filename = None
        if os.path.exists(last_branch_selected):
            self.filename = last_branch_selected
            self.branch_chooser.set_filename(self.filename)

        self.kill_signal = True

    def run(self):
        self.window.show_all()
        Gtk.main()
        return self.filename

    def on_citlali_splash_window_destroy(self, window):
        if self.kill_signal:
            self.filename = None

        Gtk.main_quit()

    def on_select_branch_button_clicked(self, chooser):
        self.filename = chooser.get_filename()
        if self.filename is not None:
            models.Settings(models.INTERNALDB).set("last_branch_selected", self.filename)
            self.kill_signal = False
            self.window.destroy()

    def on_add_branch_button_clicked(self, button):
        self.load()
        self.add_branch_window = self.builder.get_object("add_branch_window")
        self.add_branch_window.show_all()

    def on_add_branch_button_confirm_clicked(self, chooser):
        name = self.builder.get_object("add_branch_name").get_text()
        addres = self.builder.get_object("add_branch_address").get_text()
        if name.replace(" ","") == "":
            return None
        elif addres.replace(" ","") == "":
            return None
        elif chooser.get_filename() is None:
            return None
        else:
            path = os.path.join(os.path.normpath(chooser.get_filename()),"{0}.citlali".format(name.replace(" ","")))
            path = models.Branch().create(name, addres, path)
            self.branch_chooser.set_filename(path)

        self.add_branch_window.destroy()


    def on_add_branch_button_cancel_clicked(self, button):
        self.add_branch_window.destroy()

    def load(self):
        self.builder.add_from_file(os.path.join("views", "splash.glade"))
        self.builder.connect_signals(self)


