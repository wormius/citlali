# -*- coding: utf-8 -*

import gi

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk

import os

from .baseapp import CitlaliApp

import models


class Stocktaking(CitlaliApp):
    def __init__(self, branch):
        CitlaliApp.__init__(self, branch,os.path.join("views", "stocktaking.glade"))
        self.products = models.Products(self.branch)

        self.products_store = self.builder.get_object("products_store")

        self.column_cost = self.builder.get_object("column_cost")
        self.column_cost.set_cell_data_func(self.builder.get_object("cell_cost"), self.truncate,5)

        self.column_price = self.builder.get_object("column_price")
        self.column_price.set_cell_data_func(self.builder.get_object("cell_price"), self.truncate, 6)


        for p in self.products.get_all_products():
            self.products_store.append([p.id,p.category, p.name,p.details,p.guaranty,p.cost,p.price,p.stock])


    def on_add_product_button_clicked(self, button):
        row = StockTakingAddProduct(self.branch).run()
        if row is not None:
            self.products_store.append(row)

    def on_cell_category_edited(self, *args):
        selection = self.builder.get_object("products_selection")
        new = args[2]
        iter = int(args[1])
        self.products_store.set_value(selection.get_selected()[1], 1, new)
        self.products.set(self.products_store[iter][0],"category",new)

    def on_cell_name_edited(self, *args):
        selection = self.builder.get_object("products_selection")
        new = args[2]
        iter = int(args[1])
        self.products_store.set_value(selection.get_selected()[1], 2, new)
        self.products.set(self.products_store[iter][0], "name", new)

    def on_cell_details_edited(self, *args):
        selection = self.builder.get_object("products_selection")
        new = args[2]
        iter = int(args[1])
        self.products_store.set_value(selection.get_selected()[1], 3, new)
        self.products.set(self.products_store[iter][0], "details", new)

    def on_cell_guaranty_edited(self, *args):
        selection = self.builder.get_object("products_selection")
        new = args[2]
        iter = int(args[1])
        self.products_store.set_value(selection.get_selected()[1], 4, new)
        self.products.set(self.products_store[iter][0], "guaranty", new)

    def on_cell_cost_edited(self, *args):
        selection = self.builder.get_object("products_selection")
        new = args[2]
        iter = int(args[1])
        self.products_store.set_value(selection.get_selected()[1], 5, float(new))
        self.products.set(self.products_store[iter][0], "cost", float(new))

    def on_cell_price_edited(self, *args):
        selection = self.builder.get_object("products_selection")
        new = args[2]
        iter = int(args[1])
        self.products_store.set_value(selection.get_selected()[1], 6, float(new))
        self.products.set(self.products_store[iter][0], "price", float(new))

    def on_cell_stock_edited(self, *args):
        selection = self.builder.get_object("products_selection")
        new = args[2]
        iter = int(args[1])
        self.products_store.set_value(selection.get_selected()[1], 7, int(new))
        self.products.set(self.products_store[iter][0], "stock", int(new))

    def on_products_treeview_row_activated(self, treeview, path, column):
        if column is self.builder.get_object("treeview_delete_icon"):
            dialog = self.builder.get_object("delete_product_dialog")
            response = dialog.run()
            dialog.hide()
            if response == Gtk.ResponseType.OK:
                iter = self.products_store.get_iter(path)
                self.products.delete((self.products_store[iter][0]))
                self.products_store.remove(iter)

    def on_column_stock_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 7:
            self.products_store.set_sort_column_id(7, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(7, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(7, Gtk.SortType.ASCENDING)

    def on_column_cost_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 5:
            self.products_store.set_sort_column_id(5, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(5, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(5, Gtk.SortType.ASCENDING)

    def on_column_price_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 6:
            self.products_store.set_sort_column_id(6, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(6, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(6, Gtk.SortType.ASCENDING)

    def on_column_guaranty_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 4:
            self.products_store.set_sort_column_id(4, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(4, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(4, Gtk.SortType.ASCENDING)

    def on_column_details_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 3:
            self.products_store.set_sort_column_id(3, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(3, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(3, Gtk.SortType.ASCENDING)

    def on_column_name_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 2:
            self.products_store.set_sort_column_id(2, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(2, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(2, Gtk.SortType.ASCENDING)

    def on_column_category_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 1:
            self.products_store.set_sort_column_id(1, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(1, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(1, Gtk.SortType.ASCENDING)

    def on_column_id_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 0:
            self.products_store.set_sort_column_id(0, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(0, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(0, Gtk.SortType.ASCENDING)


class StockTakingAddProduct(CitlaliApp):
    def __init__(self, branch):
        CitlaliApp.__init__(self, branch, os.path.join("views", "stocktaking_add_window.glade"))
        self.products = models.Products(self.branch)

        self.category_combo = self.builder.get_object("category_combo")
        for c in self.products.get_all_categories():
            self.category_combo.append_text(c)
        self.name_entry = self.builder.get_object("name_entry")
        self.details_entry = self.builder.get_object("details_entry")
        self.guaranty_entry = self.builder.get_object("guaranty_entry")
        self.cost_spin = self.builder.get_object("cost_spinbutton")
        self.price_spin = self.builder.get_object("price_spinbutton")
        self.stock_spin = self.builder.get_object("stock_spinbutton")

    def on_add_button_confirm_clicked(self, button):
        category = self.category_combo.get_active_text()
        name = self.name_entry.get_text()
        details = self.details_entry.get_text()
        guaranty = self.guaranty_entry.get_text()
        cost = self.cost_spin.get_text()
        price = self.price_spin.get_text()
        stock = self.stock_spin.get_text()

        fields = [category,name,details,guaranty,cost,price,stock]
        none_flag = False

        for f in fields:
            if f.replace(" ","") == "":
                none_flag = True
                break


        if not none_flag:
            try:
                cost = float(cost.replace(",","."))
                price = float(price.replace(",","."))
                stock = int(stock.replace(",","."))
                id = self.products.add(category,name,details,guaranty,cost,price,stock)
                self.app_response = [id,category,name,details,guaranty,cost,price,stock]
                self.kill()
            except:
                pass


