# -*- coding: utf-8 -*


from .baseapp import CitlaliApp
from .splash import Splash
from .showcase import Showcase
from .stocktaking import Stocktaking
from .summary import Summary
from .sellers import Sellers
from .access import Password

import gi

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk

import os

import sys

import models


class Main(CitlaliApp):

    def __init__(self, branch):
        CitlaliApp.__init__(self,branch,os.path.join("views", "main.glade"))

        self.selected_app = {}

        self.branch_label = self.builder.get_object("branch_label")
        self.branch_label.set_text(models.Settings(self.branch).get("name"))


    def on_showcase_button_clicked(self, *args):
        if not "showcase" in self.selected_app.keys():
            self.selected_app["showcase"] = Showcase(self.branch)
            self.selected_app["showcase"].run()
            del(self.selected_app["showcase"])

    def on_stocktaking_button_clicked(self, *args):
        if not "stocktaking" in self.selected_app.keys():
            if Password(self.branch).run():
                self.selected_app["stocktaking"] = Stocktaking(self.branch)
                self.selected_app["stocktaking"].run()
                del (self.selected_app["stocktaking"])

    def on_summary_button_clicked(self, *args):
        if not "summary" in self.selected_app.keys():
            if Password(self.branch).run():
                self.selected_app["summary"] = Summary(self.branch)
                self.selected_app["summary"].run()
                del (self.selected_app["summary"])

    def on_seller_button_clicked(self, *args):
        if not "sellers" in self.selected_app.keys():
            if Password(self.branch).run():
                self.selected_app["sellers"] = Sellers(self.branch)
                self.selected_app["sellers"].run()
                del (self.selected_app["sellers"])

    def on_main_window_destroy(self, window):
        for k in self.selected_app.keys():
            self.selected_app[k].kill()

        self.kill()
        sys.exit(0)



