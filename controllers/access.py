# -*- coding: utf-8 -*


import os

import gi

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk
from gi.repository import Gdk

from .baseapp import CitlaliApp

import models


class Password(CitlaliApp):

    def __init__(self, branch):
        CitlaliApp.__init__(self,branch, os.path.join("views","password.glade"))
        self.settings = models.Settings(self.branch)
        self.real_password = self.settings.get("password")
        self.password_entry = self.builder.get_object("password_entry")
        self.password_label = self.builder.get_object("label")
        self.app_response = False

    def on_password_button_accept_clicked(self,button):
        password = self.password_entry.get_text()
        if password is not None:
            if password.replace(" ","") != "":
                if self.real_password == ";":
                    self.settings.set("password",password+";")
                    self.app_response = True
                    self.kill()
                else:
                    if self.real_password == password+";":
                        self.app_response = True
                        self.kill()
                    else:
                        self.password_entry.set_text("")
                        self.password_label.set_text("Intente de nuevo:   ")

    def on_main_window_key_press_event(self, *args):
        if args[1].keyval == Gdk.KEY_Return:
            self.on_password_button_accept_clicked(None)
        elif args[1].keyval == Gdk.KEY_Escape:
            self.kill()