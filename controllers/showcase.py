# -*- coding: utf-8 -*

import gi

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk

import os

from .baseapp import CitlaliApp

import models

import catrina

class Showcase(CitlaliApp):

    def __init__(self, branch):
        CitlaliApp.__init__(self,branch, os.path.join("views","showcase.glade"))
        self.products = models.Products(self.branch)

        self.search_entry = self.builder.get_object("checker_searchentry")

        self.pname = self.builder.get_object("checker_product_name")
        self.pdetails = self.builder.get_object("checker_product_details")
        self.pguaranty = self.builder.get_object("checker_product_guaranty")
        self.pprice = self.builder.get_object("checker_product_price")


    def on_checker_searchentry_search_changed(self, *args):
        pid = self.search_entry.get_text()
        data = None
        if pid.replace(" ","") != "":
            try:
                pid = int(pid)
            except:
                return

            data = self.products.get_product(pid)

        if data is not None:
            self.pname.set_text(data.name)
            self.pdetails.set_text(data.details)
            self.pguaranty.set_text(data.guaranty)
            self.pprice.set_text("$ {0} MXN".format(data.price))
        else:
            self.pname.set_text("Product's name")
            self.pdetails.set_text("Product's details")
            self.pguaranty.set_text("Product's guaranty")
            self.pprice.set_text("Price")

    def on_product_list_button_clicked(self, button):
        productid = ShowcaseProductList(self.branch).run()
        self.search_entry.set_text("{0}".format(productid))

    def on_new_sale_button_clicked(self, button):
        ShowcaseNewSale(self.branch).run()


class ShowcaseNewSaleConfirm(CitlaliApp):

    def __init__(self, branch, sale_data, content):
        CitlaliApp.__init__(self, branch, os.path.join("views", "showcase_new_sale_confirm.glade"))
        self.products = models.Products(self.branch)
        self.app_response = False, ""
        self.buffer = self.builder.get_object("buffer")
        tpl_main = """
=================================================
                   Productos
=================================================

{{products}}

{{subtotal}}
{{invoice}}
{{card}}
{{total}}

        """

        text_products = ""
        for product in content:
            product_data = self.products.get_product(product[0])

            name = product_data.category+" "+product_data.name
            if len(name) > 35:
                name = name[:35]+"..."

            for i in range(48 - len(name) - len(str(product[4]*product[3])) -len(str(product[3]))):
                name += " "

            variant = product[1]
            if len(variant) > 35:
                variant = variant[:35]+"..."

            details = product_data.details
            if len(details) > 35:
                details = details[:35]+"..."

            line = "{0} {1}{2}\n{3}\n{4}\n".format(product[3],name,product[4]*product[3],details, variant)
            text_products += "{0}\n".format(line)





        tpl_text = catrina.TextTemplate(tpl_main)
        tpl_text.products = text_products

        if sale_data[0]:
            cardlabel ="        Uso de terminal 5%"
            fill = ""
            for i in range(48 - len(cardlabel) - len(str(round(sale_data[2]*0.05,1)))):
                fill += " "
            tpl_text.card = "{0}{1}{2}".format(cardlabel,fill,round(sale_data[2]*0.05,1))
        else:
            tpl_text.card=""


        if sale_data[1]:
            invoicelabel ="        IVA 16%"
            fill = ""
            for i in range(48 - len(invoicelabel) - len(str(round(sale_data[2]*0.16,1)))):
                fill += " "
            tpl_text.invoice = "{0}{1}{2}".format(invoicelabel,fill,round(sale_data[2]*0.16,1))
        else:
            tpl_text.invoice=""

        subtotallabel = "        SUBTOTAL"
        fill = ""
        for i in range(48 - len(subtotallabel) - len(str(sale_data[2]))):
            fill += " "
        tpl_text.subtotal = "{0}{1}{2}".format(subtotallabel, fill, sale_data[2])

        totallabel = "        TOTAL"
        fill = ""
        for i in range(48 - len(totallabel) - len(str(sale_data[3]))):
            fill += " "
        tpl_text.total = "{0}{1}{2}".format(totallabel, fill, sale_data[3])


        self.ticket = catrina.TextTemplate(tpl_text.compile())


        self.buffer.set_text(self.ticket.compile())

    def on_button_accept_clicked(self, button):
        self.app_response = True, self.ticket.compile()
        self.kill()

class ShowcaseNewSale(CitlaliApp):

    def __init__(self, branch):
        CitlaliApp.__init__(self,branch, os.path.join("views","showcase_new_sale.glade"))
        self.products = models.Products(self.branch)
        self.sales = models.Sales(self.branch)
        self.sellers = models.Sellers(self.branch)

        self.sale_store = self.builder.get_object("sale_store")

        self.column_unit_price = self.builder.get_object("column_unit_price")
        self.column_unit_price.set_cell_data_func(self.builder.get_object("cell_unit_price"), self.truncate, 2)

        self.column_total_price = self.builder.get_object("column_total_price")
        self.column_total_price.set_cell_data_func(self.builder.get_object("cell_total_price"), self.truncate, 3)

        self.label_total_price = self.builder.get_object("sale_total_price")

        self.card = self.builder.get_object("checkbutton_card")
        self.invoice = self.builder.get_object("checkbutton_invoice")

        self.sale_details_entry = self.builder.get_object("sale_details")
        self.seller_combo = self.builder.get_object("combo_seller")
        for s in self.sellers.get_actives():
            self.seller_combo.append("sellerid_{0}".format(s.id),s.name)


        self.subtotal_of_sale = 0.0
        self.total_of_sale = 0.0

    def on_new_sale_confirm_button_clicked(self,button):

        if len(self.sale_store) > 0:

            sale_details = self.sale_details_entry.get_text()
            if sale_details is None:
                sale_details =""

            sale_data = (self.card.get_active(),self.invoice.get_active(),self.subtotal_of_sale, self.total_of_sale, sale_details)
            content = []

            for row in self.sale_store:
                content.append((row[4],row[5],row[6],row[0],row[2]))

            response = ShowcaseNewSaleConfirm(self.branch,sale_data,content).run()
            if response[0]:
                ticket = response[1]
                sellerid = self.seller_combo.get_active_id()
                if sellerid is not None:
                    sellerid = int(sellerid.split("_")[1])
                else:
                    sellerid = -1

                self.sales.add(sellerid,sale_data,content)
                self.kill()



    def on_add_button_clicked(self,button):
        data = ShowcaseNewSaleAddProduct(self.branch).run()

        if data is not None:
            product_id = data[0]
            variant = data[1]
            price = data[2]
            details = data[3]

            product = self.products.get_product(product_id)

            if variant is None:
                variant = ""

            if details is None:
                details = ""

            self.sale_store.append([1,product.category+" "+product.name,price, price, product_id, variant, details])


    def on_cell_volume_edited(self, *args):
        selection = self.builder.get_object("sale_selection")
        new = args[2]
        iter = int(args[1])
        self.sale_store.set_value(selection.get_selected()[1], 0, int(new))
        self.sale_store[iter][3] = self.sale_store[iter][2]*int(new)

        self.update_total()

    def on_checkbutton_card_toggled(self, *args):
        self.update_total()

    def on_checkbutton_invoice_toggled(self, *args):
        self.update_total()

    def update_total(self, *args):
        subtotal = 0.0
        for row in self.sale_store:
            subtotal += row[3]

        self.subtotal_of_sale = round(subtotal,1)

        total = self.subtotal_of_sale

        if self.invoice.get_active():
            total = total*1.16

        if self.card.get_active():
            total = total*1.05


        self.total_of_sale = round(total,1)

        self.label_total_price.set_text(str(self.total_of_sale))


    def on_sale_treeview_row_activated(self, treeview, path, column):
        if column is self.builder.get_object("column_delete_icon"):
            iter = self.sale_store.get_iter(path)
            self.sale_store.remove(iter)






class ShowcaseNewSaleAddProduct(CitlaliApp):

    def __init__(self, branch):
        CitlaliApp.__init__(self,branch, os.path.join("views","showcase_new_sale_add_product.glade"))

        self.products = models.Products(self.branch)

        self.entry_id = self.builder.get_object("searchentry")
        self.pname = self.builder.get_object("product_name")
        self.pdetails = self.builder.get_object("product_details")

        self.pprice = self.builder.get_object("price_entry")
        self.pvariant = self.builder.get_object("variant_combo")



        self.pdata = self.builder.get_object("data_entry")

    def on_products_list_button_clicked(self, *args):
        productid = ShowcaseProductList(self.branch).run()
        self.entry_id.set_text(str(productid))

    def on_searchentry_search_changed(self, *args):
        pid = self.entry_id.get_text()
        data = None
        if pid.replace(" ", "") != "":
            try:
                pid = int(pid)
            except:
                return

            data = self.products.get_product(pid)

        if data is not None:
            self.pname.set_text(data.name)
            self.pdetails.set_text(data.details)
            self.pvariant.remove_all()
            for v in self.products.get_variants(pid):
                self.pvariant.append_text(v)
        else:
            self.pname.set_text("Product's name")
            self.pdetails.set_text("Product's details")

    def on_add_confirm_button_clicked(self, button):
        pid = self.entry_id.get_text()
        data = None
        if pid.replace(" ", "") != "":
            try:
                pid = int(pid)
            except:
                return

            data = self.products.get_product(pid)

        if data is not None:
            price = self.pprice.get_text()

            if price is None:
                price = data.price()
            elif price.replace(" ","") == "":
                price = data.price
            else:
                try:
                    price = float(price)
                except:
                    return

            variant = self.pvariant.get_active_text()

            if variant.replace(" ","") == "":
                variant = None


            if variant is not None:
                add_variant_flag = True
                for v in self.products.get_variants(data.id):
                    if variant==v:
                        add_variant_flag = False

                if add_variant_flag:
                    self.products.add_variant(data.id, variant)


            pdata = self.pdata.get_text()

            if pdata.replace(" ", "") == "":
                pdata = None

            self.app_response = [data.id, variant, price, pdata]
            self.kill()


class ShowcaseProductList(CitlaliApp):

    def __init__(self, branch):
        CitlaliApp.__init__(self,branch, os.path.join("views","showcase_productslist.glade"))
        self.products = models.Products(self.branch)

        self.products_store = self.builder.get_object("products_store")

        self.column_price = self.builder.get_object("column_price")
        self.column_price.set_cell_data_func(self.builder.get_object("cell_price"), self.truncate, 5)

        for p in self.products.get_all_products():
            self.products_store.append([p.id, p.category, p.name, p.details, p.guaranty, p.price, p.stock])

    def on_products_treeview_row_activated(self, treeview, path, column):
        iter = self.products_store.get_iter(path)
        self.app_response = self.products_store[iter][0]
        self.kill()

    def on_column_stock_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 6:
            self.products_store.set_sort_column_id(6, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(6, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(6, Gtk.SortType.ASCENDING)

    def on_column_price_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 5:
            self.products_store.set_sort_column_id(5, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(5, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(5, Gtk.SortType.ASCENDING)

    def on_column_guaranty_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 4:
            self.products_store.set_sort_column_id(4, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(4, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(4, Gtk.SortType.ASCENDING)

    def on_column_details_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 3:
            self.products_store.set_sort_column_id(3, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(3, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(3, Gtk.SortType.ASCENDING)

    def on_column_name_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 2:
            self.products_store.set_sort_column_id(2, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(2, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(2, Gtk.SortType.ASCENDING)

    def on_column_category_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 1:
            self.products_store.set_sort_column_id(1, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(1, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(1, Gtk.SortType.ASCENDING)

    def on_column_id_clicked(self, *args):
        column, sort_type = self.products_store.get_sort_column_id()
        if column != 0:
            self.products_store.set_sort_column_id(0, Gtk.SortType.ASCENDING)
        elif sort_type == Gtk.SortType.ASCENDING:
            self.products_store.set_sort_column_id(0, Gtk.SortType.DESCENDING)
        else:
            self.products_store.set_sort_column_id(0, Gtk.SortType.ASCENDING)

