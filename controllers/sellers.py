# -*- coding: utf-8 -*

import gi

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk

import os

import models


from .baseapp import CitlaliApp

class Sellers(CitlaliApp):

    def __init__(self, branch):
        CitlaliApp.__init__(self, branch, os.path.join("views", "sellers.glade"))
        self.sellers = models.Sellers(self.branch)

        self.store = self.builder.get_object("sellers_store")

        self.column_commission = self.builder.get_object("column_seller_commission")
        self.column_commission.set_cell_data_func(self.builder.get_object("cell_seller_commission"), self.truncate, 2)

        for s in self.sellers.get_actives():
            self.store.append([s.id, s.name, s.commission*100])

    def on_add_seller_button_clicked(self, button):
        row = SellerAdd(self.branch).run()
        if row is not None:
            self.store.append(row)

    def on_cell_seller_name_edited(self, *args):
        selection = self.builder.get_object("seller_selection")
        new = args[2]
        iter = int(args[1])
        self.store.set_value(selection.get_selected()[1], 1, new)
        self.sellers.edit_name(self.store[iter][0],new)

    def on_cell_seller_commission_edited(self, *args):
        selection = self.builder.get_object("seller_selection")
        new = args[2]
        iter = int(args[1])
        self.store.set_value(selection.get_selected()[1], 2, float(new))
        self.sellers.edit_commission(self.store[iter][0],float(new)/100.0)

    def on_sellers_treeview_row_activated(self, treeview, path, column):
        if column is self.builder.get_object("treeviewcolumn_delete_icon"):
            dialog = self.builder.get_object("delete_seller_dialog")
            response = dialog.run()
            dialog.hide()
            if response == Gtk.ResponseType.OK:
                iter = self.store.get_iter(path)
                self.sellers.delete((self.store[iter][0]))
                self.store.remove(iter)

class SellerAdd(CitlaliApp):

    def __init__(self, branch):
        CitlaliApp.__init__(self, branch, os.path.join("views", "sellers_add_window.glade"))
        self.sellers = models.Sellers(self.branch)

    def on_add_seller_button_accept_clicked(self, button):
        name = self.builder.get_object("add_seller_entry").get_text()
        commission = self.builder.get_object("add_seller_spinbutton").get_text()
        if name.replace(" ","") != "":
            id = self.sellers.add(name,float(commission)/100.0)
            self.app_response = [id, name, float(commission)]
            self.kill()

