# -*- coding: utf-8 -*

import gi

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk

import os

from datetime import datetime

from .baseapp import CitlaliApp

import models




class Summary(CitlaliApp):

    def __init__(self, branch):
        CitlaliApp.__init__(self,branch, os.path.join("views","summary.glade"))
        self.settings = models.Settings(self.branch)
        self.sellers = models.Sellers(self.branch)
        self.sales = models.Sales(self.branch)

        self.sales_treeview = self.builder.get_object("sales_treeview")
        self.sales_liststore = self.builder.get_object("sales_liststore")

        self.column_total = self.builder.get_object("column_total")
        self.column_total.set_cell_data_func(self.builder.get_object("cell_total"), self.truncate, 4)

        self.date = datetime.now().day, datetime.now().month, datetime.now().year

        self.date_label = self.builder.get_object("label_date")
        self.n_sales_label = self.builder.get_object("label_n_sales")
        self.total_label = self.builder.get_object("label_total")
        self.cash_label = self.builder.get_object("label_cash")
        self.card_label = self.builder.get_object("label_card")
        self.n_invoice_label = self.builder.get_object("label_n_invoice")


        self.update_summary()


    def update_summary(self):

        self.date_label.set_text("{0}/{1}/{2}".format(*self.date))
        all_sales = self.sales.get_by_date(*self.date)
        n_sales = len(all_sales)
        self.n_sales_label.set_text("{0}".format(n_sales))

        total = 0.0
        for s in all_sales:
            total+= s.total

        self.total_label.set_text("{0}".format(round(total,1)))

        total = 0.0
        for s in all_sales:
            if s.card == 0:
                total += s.total
        self.cash_label.set_text("{0}".format(round(total,1)))

        total = 0.0
        for s in all_sales:
            if s.card == 1:
                total += s.total
        self.card_label.set_text("{0}".format(round(total,1)))

        total = 0
        for s in all_sales:
            if s.invoice == 1:
                total += 1
        self.n_invoice_label.set_text("{0}".format(total))


        self.sales_liststore.clear()
        for s in all_sales:
            seller = self.sellers.get(s.sellerid)
            name = ""
            if seller is None:
                name = ""
            else:
                name = seller.name

            card = "Sí" if s.card == 1 else ""
            invoice = "Sí" if s.invoice == 1 else ""
            self.sales_liststore.append([s.id, name, card, invoice, s.total])



    def on_sales_treeview_row_activated(self, treeview, path, column):
        if column is self.builder.get_object("treeview_delete_icon"):
            dialog = self.builder.get_object("delete_product_dialog")
            response = dialog.run()
            dialog.hide()
            if response == Gtk.ResponseType.OK:
                iter = self.sales_liststore.get_iter(path)
                self.sales.cancel((self.sales_liststore[iter][0]))
                self.sales_liststore.remove(iter)

        elif column is self.builder.get_object("treeview_more_icon"):
            iter = self.sales_liststore.get_iter(path)
            saleid = self.sales_liststore[iter][0]
            SaleDetails(self.branch, saleid).run()



    def on_button_commissions_clicked(self, button):
        Commissions(self.branch, self.date).run()

    def on_cell_seller_edited(self, *args):
        selection = self.builder.get_object("sales_selection")
        new = args[2]
        iter = int(args[1])

        try:
            int(new)
        except:
            return

        seller = self.sellers.get(int(new))
        if seller is not None:
            self.sales_liststore.set_value(selection.get_selected()[1], 1, seller.name)
            self.sales.set_seller(self.sales_liststore[iter][0], seller.id)

    def on_button_calendar_clicked(self, button):
        self.date = Calendar(self.branch).run()
        self.update_summary()


class SaleDetails(CitlaliApp):
    def __init__(self, branch, saleid):
        CitlaliApp.__init__(self,branch, os.path.join("views","summary_sale_details.glade"))
        self.sales = models.Sales(self.branch)
        self.sellers = models.Sellers(self.branch)
        self.products = models.Products(self.branch)

        self.store = self.builder.get_object("store")

        self.column_cost = self.builder.get_object("column_cost")
        self.column_cost.set_cell_data_func(self.builder.get_object("cell_cost"), self.truncate, 5)

        self.column_price = self.builder.get_object("column_price")
        self.column_price.set_cell_data_func(self.builder.get_object("cell_price"), self.truncate, 6)

        self.column_total = self.builder.get_object("column_total")
        self.column_total.set_cell_data_func(self.builder.get_object("cell_total"), self.truncate, 7)

        self.buffer = self.builder.get_object("buffer")
        self.subtotal = self.builder.get_object("label_subtotal")

        sale = self.sales.get_by_id(saleid)

        if sale is not None:
            for c in self.sales.get_content_of_sale(sale.id):
                product = self.products.get_product(c.productid)
                self.store.append([c.volume,product.id,product.category,product.name,c.variant,product.cost,c.price,c.volume*c.price, c.data])

            self.buffer.set_text(sale.details if sale.details.replace(" ","") != "" else "Sin observaciones")
            self.subtotal.set_text("SUBTOTAL:    {0}".format(sale.subtotal))
        else:
            self.kill()

class Commissions(CitlaliApp):
    def __init__(self, branch, date):
        CitlaliApp.__init__(self,branch, os.path.join("views","commissions.glade"))
        self.sales = models.Sales(self.branch)
        self.sellers = models.Sellers(self.branch)
        self.products = models.Products(self.branch)
        self.date = date

        self.store = self.builder.get_object("commissions_store")

        self.column_commission = self.builder.get_object("column_commission")
        self.column_commission.set_cell_data_func(self.builder.get_object("cell_commission"), self.truncate, 1)

        sales = self.sales.get_by_date(*self.date)
        sellerids = []
        for s in sales:
            sellerids.append(s.sellerid)

        sellerids = list(set(sellerids))

        for id in sellerids:
            if id != -1:
                seller = self.sellers.get(id)
                if seller is not None:
                    total = 0.0
                    for s in self.sales.get_by_date_and_seller(seller.id, *self.date):
                        for c in self.sales.get_content_of_sale(s.id):
                            product = self.products.get_product(c.productid)
                            total += c.volume*(c.price- product.cost)

                    self.store.append([seller.name, total*seller.commission])







class Calendar(CitlaliApp):
    def __init__(self, branch):
        CitlaliApp.__init__(self,branch, os.path.join("views","summary_calendar.glade"))
        self.app_response = datetime.now().day, datetime.now().month, datetime.now().year

        self.calendar = self.builder.get_object("calendar")
        self.calendar.select_day(datetime.now().day)
        self.calendar.select_month(datetime.now().month-1, datetime.now().year)


    def on_calendar_day_selected_double_click(self, *args):
        date = self.calendar.get_date()
        self.app_response = date.day, date.month+1, date.year
        self.kill()