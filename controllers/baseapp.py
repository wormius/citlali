# -*- coding: utf-8 -*

import gi

gi.require_version("Gtk", "3.0")

from gi.repository import Gtk


class CitlaliApp(object):
    def __init__(self, branch, view):
        self.branch = branch
        self.view = view
        self.app_response =None

        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.view)
        self.builder.connect_signals(self)

        self.window = self.builder.get_object("main_window")

    def run(self):
        self.window.show_all()
        Gtk.main()

        return self.app_response

    def kill(self, *args):
        self.window.destroy()
        Gtk.main_quit()

    def truncate(self,tree_column, cell, tree_model, iter, *data):
        number = round(tree_model[iter][data[0]],2)
        cell.set_property('text', str(number))
        return