# -*- coding: utf-8 -*

from datetime import datetime

import os

import catrina

INTERNALDB = "internal"


def check_init():
    if not os.path.exists(os.path.join(".", INTERNALDB)):
        catrina.create(os.path.join(".", INTERNALDB))

        sql = """
                CREATE TABLE settings 
                (
                    key TEXT PRIMARY KEY,
                    value TEXT
                );

                INSERT INTO settings
                VALUES
                (
                    "init",
                    "1"
                );
                INSERT INTO settings
                VALUES
                (
                    "last_branch_selected",
                    ""
                );
              """

        create = catrina.Script(sql, INTERNALDB)
        create.execute()

    return True


class Settings(object):

    def __init__(self, database):
        self.__query_select = catrina.Query("SELECT value FROM settings WHERE key='{{key}}';", database, "value")
        self.__query_update = catrina.Query("UPDATE settings SET value='{{value:text}}' WHERE key='{{key}}';", database)
        self.__query_insert = catrina.Query("INSERT INTO settings VALUES ('{{key}}', '{{value:text}}');", database)

    def get(self, key):
        self.__query_select.key = key
        value = self.__query_select.execute()[0]
        if value is not None:
            return self.__query_select.execute()[0].value
        else:
            return value

    def set(self, key, value):
        self.__query_update.key = key
        self.__query_update.value = value
        self.__query_update.execute()

    def add(self, key, value):
        self.__query_insert.key = key
        self.__query_insert.value = value
        self.__query_insert.execute()


class Branch(object):

    def create(self, name, address, path):
        path = catrina.create(path)
        sql = """
                CREATE TABLE settings 
                (
                    key TEXT PRIMARY KEY,
                    value TEXT
                );
                                
                CREATE TABLE products
                (
                    id INTEGER PRIMARY KEY,
                    category TEXT,
                    name TEXT,
                    details TEXT,
                    guaranty TEXT,
                    cost REAL,
                    price REAL,
                    stock INTEGER,
                    active INTEGER
                );
                
                CREATE TABLE variants
                (
                    productid INTEGER,
                    name TEXT 
                );
                
                CREATE TABLE sellers
                (
                    id INTEGER PRIMARY KEY,
                    active INTEGER,
                    name TEXT,
                    commission REAL
                );
                
                CREATE TABLE sales
                (
                    id INTEGER PRIMARY KEY,
                    sellerid INTEGER,
                    minute INTEGER,
                    hour INTEGER,                    
                    day INTEGER,
                    month INTEGER,
                    year INTEGER,
                    card INTEGER,
                    invoice INTEGER,
                    subtotal REAL,
                    total REAL,
                    details TEXT
                );
                
                CREATE TABLE sale_content
                (
                    saleid INTEGER ,
                    productid INTEGER,
                    variant TEXT,
                    data TEXT,
                    volume INTEGER,
                    price REAL                    
                );                          
              """
        script = catrina.Script(sql, path)
        script.execute()

        settings = Settings(path)
        settings.add("name", name)
        settings.add("address", address)
        settings.add("last_product_id", 0)
        settings.add("last_seller_id", 100)
        settings.add("last_sale_id", 0)
        settings.add("password", ";")

        Sellers(path).add("Default", 0.0)

        return path


class Sales(object):
    def __init__(self, branch):
        self.branch = branch
        self.settings = Settings(self.branch)
        self.products = Products(self.branch)


        self.__query_sale_add = catrina.Query("""INSERT INTO sales
                                                 VALUES
                                                 (
                                                    {{id:integer}},
                                                    {{sellerid:integer}},
                                                    {{minute:integer}},
                                                    {{hour:integer}},                    
                                                    {{day:integer}},
                                                    {{month:integer}},
                                                    {{year:integer}},
                                                    {{card:integer}},
                                                    {{invoice:integer}},
                                                    {{subtotal:real}},
                                                    {{total:real}},
                                                    '{{details:text}}'
                                                 );""", self.branch)

        self.__query_content_add = catrina.Query("""INSERT INTO sale_content
                                                    VALUES
                                                    (
                                                        {{saleid:integer}},
                                                        {{productid:integer}},
                                                        '{{variant:text}}',
                                                        '{{data:text}}',
                                                        {{volume:integer}},
                                                        {{price:real}}
                                                    );""",self.branch)

        self.__query_sale_get_by_date = catrina.Query("""
                                                        SELECT id, sellerid, minute, hour, day, month, year, card, invoice, subtotal, total, details
                                                        FROM sales
                                                        WHERE day={{day:integer}} AND month={{month:integer}} AND year = {{year:integer}};
                                                        """, self.branch,
                                                      ("id","sellerid","minute","hour","day","month","year","card","invoice","subtotal","total","details"))

        self.__query_sale_get_by_date_and_seller = catrina.Query("""
                                                                SELECT id, sellerid, minute, hour, day, month, year, card, invoice, subtotal, total, details
                                                                FROM sales
                                                                WHERE sellerid={{sellerid:integer}} AND day={{day:integer}} AND month={{month:integer}} AND year = {{year:integer}};
                                                                """, self.branch,
                                                      ("id", "sellerid", "minute", "hour", "day", "month", "year",
                                                       "card", "invoice", "subtotal", "total", "details"))

        self.__query_sale_get_content_of_sale = catrina.Query("""
                                                                SELECT saleid, productid, variant, data, volume, price
                                                                FROM sale_content
                                                                WHERE saleid={{saleid:integer}};
                                                                """, self.branch,
                                                                 ("saleid","productid","variant","data","volume","price"))

        self.__query_set_seller = catrina.Query("UPDATE sales SET sellerid={{sellerid:integer}} WHERE id={{id:integer}};", self.branch)

        self.__query_cancel_delete_sale = catrina.Query("DELETE FROM sales WHERE id={{id:integer}};", self.branch)
        self.__query_cancel_get_content = catrina.Query("SELECT productid, volume FROM sale_content WHERE saleid={{saleid:integer}};", self.branch, ("productid","volume"))
        self.__query_cancel_delete_content = catrina.Query("DELETE FROM sale_content WHERE saleid={{saleid:integer}};", self.branch)

        self.__query_sale_get_by_id = catrina.Query("""
                                                    SELECT id, sellerid, minute, hour, day, month, year, card, invoice, subtotal, total, details
                                                    FROM sales
                                                    WHERE id = {{id:integer}};
                                                    """, self.branch,
                                                      ("id", "sellerid", "minute", "hour", "day", "month", "year",
                                                       "card", "invoice", "subtotal", "total", "details"))

    def get_by_id(self, saleid):
        self.__query_sale_get_by_id.id = saleid
        sale = self.__query_sale_get_by_id.execute()
        if len(sale) > 0:
            return sale[0]
        else:
            return None

    def get_by_date(self, day, month, year):
        self.__query_sale_get_by_date.day = day
        self.__query_sale_get_by_date.month = month
        self.__query_sale_get_by_date.year = year
        return self.__query_sale_get_by_date.execute()

    def get_by_date_and_seller(self, seller_id, day, month, year):
        self.__query_sale_get_by_date_and_seller.sellerid = seller_id
        self.__query_sale_get_by_date_and_seller.day = day
        self.__query_sale_get_by_date_and_seller.month = month
        self.__query_sale_get_by_date_and_seller.year = year
        return self.__query_sale_get_by_date_and_seller.execute()

    def get_content_of_sale(self, sale_id):
        self.__query_sale_get_content_of_sale.saleid = sale_id
        return self.__query_sale_get_content_of_sale.execute()

    def set_seller(self, sale_id, seller_id):
        self.__query_set_seller.sellerid = seller_id
        self.__query_set_seller.id = sale_id
        self.__query_set_seller.execute()

    def cancel(self, saleid):

        self.__query_cancel_get_content.saleid = saleid
        content = self.__query_cancel_get_content.execute()
        for c in content:
            p = self.products.get_product(c.productid)
            self.products.set(p.id,"stock",p.stock+c.volume)

        self.__query_cancel_delete_content.saleid = saleid
        self.__query_cancel_delete_content.execute()


        self.__query_cancel_delete_sale.id = saleid
        self.__query_cancel_delete_sale.execute()


    def add(self, sellerid, sale_data, content):

        saleid = int(self.settings.get("last_sale_id")) + 1

        time = datetime.now().time()
        date = datetime.now()

        minute = time.minute
        hour = time.hour
        day = date.day
        month = date.month
        year = date.year

        card = 1 if sale_data[0] else 0
        invoice = 1 if sale_data[1] else 0
        subtotal = sale_data[2]
        total = sale_data[3]
        sale_details = sale_data[4]

        self.__query_sale_add.id = saleid
        self.__query_sale_add.sellerid = sellerid
        self.__query_sale_add.minute = minute
        self.__query_sale_add.hour = hour
        self.__query_sale_add.day = day
        self.__query_sale_add.month = month
        self.__query_sale_add.year = year
        self.__query_sale_add.card = card
        self.__query_sale_add.invoice = invoice
        self.__query_sale_add.subtotal = subtotal
        self.__query_sale_add.total = total
        self.__query_sale_add.details = sale_details

        self.__query_sale_add.execute()


        for product in content:
            self.__query_content_add.saleid = saleid
            self.__query_content_add.productid = product[0]
            self.__query_content_add.variant = product[1]
            self.__query_content_add.data = product[2]
            self.__query_content_add.volume = product[3]
            self.__query_content_add.price = product[4]
            self.__query_content_add.execute()
            self.products.set(self.__query_content_add.productid,"stock",
                              self.products.get_product(self.__query_content_add.productid).stock - self.__query_content_add.volume)

        self.settings.set("last_sale_id", saleid)


        return saleid


class Products(object):
    def __init__(self, branch):
        self.branch = branch
        self.settings = Settings(self.branch)
        self.__query_get_all_categories = catrina.Query("SELECT category FROM products WHERE active=1;", self.branch, "category")
        self.__query_add = catrina.Query("""INSERT INTO products 
                                            VALUES
                                            (
                                                {{id:integer}},
                                                '{{category:text}}',
                                                '{{name:text}}',
                                                '{{details:text}}',
                                                '{{guaranty:text}}',
                                                {{cost:real}},
                                                {{price:real}},
                                                {{stock:integer}},
                                                1
                                            );""", self.branch, "category")
        self.__query_get_all_products = catrina.Query("SELECT * FROM products WHERE active=1;", self.branch,
                                                      ("id", "category", "name", "details", "guaranty", "cost", "price",
                                                       "stock","active"))
        self.__query_force_get_all_products = catrina.Query("SELECT * FROM products;", self.branch,
                                                      ("id", "category", "name", "details", "guaranty", "cost", "price",
                                                       "stock", "active"))

        self.__query_set_text_key = catrina.Query(
            "UPDATE products SET {{key}}='{{value:text}}' WHERE id={{id:integer}};", self.branch)
        self.__query_set_real_key = catrina.Query("UPDATE products SET {{key}}={{value:real}} WHERE id={{id:integer}};",
                                                  self.branch)
        self.__query_set_int_key = catrina.Query(
            "UPDATE products SET {{key}}={{value:integer}} WHERE id={{id:integer}};", self.branch)

        self.__query_delete = catrina.Query("UPDATE products SET active=0, stock=0 WHERE id={{id:integer}};",self.branch)

        self.__query_get_product = catrina.Query("SELECT * from products WHERE id={{id:integer}};", self.branch, ("id", "category", "name", "details", "guaranty", "cost", "price","stock","active"))

        self.__query_get_variants = catrina.Query("SELECT name FROM variants WHERE productid={{id:integer}};", self.branch, "variant")
        self.__query_add_variant = catrina.Query("INSERT INTO variants VALUES ({{id:integer}}, '{{name:text}}');", self.branch)


    def get_all_categories(self):
        categories = []
        for c in self.__query_get_all_categories.execute():
            if not (c.category in categories):
                categories.append(c.category)
        return categories

    def get_all_products(self, only_actives=True):
        if only_actives:
            return self.__query_get_all_products.execute()
        else:
            return self.__query_force_get_all_products.execute()


    def get_variants(self, id):
        self.__query_get_variants.id = id
        variants_row = self.__query_get_variants.execute()
        variants = []
        for v in variants_row:
            variants.append(v.variant)

        return variants

    def add_variant(self, id, variant):
        self.__query_add_variant.id = id
        self.__query_add_variant.name = variant
        self.__query_add_variant.execute()

    def get_product(self, id):
        self.__query_get_product.id = id
        product = self.__query_get_product.execute()
        if len(product) > 0:
            return product[0]
        else:
            return None

    def set(self, id, key, value):

        text_keys = ["category", "name", "details", "guaranty"]
        real_keys = ["cost", "price"]

        if key in text_keys:
            self.__query_set_text_key.id = id
            self.__query_set_text_key.key = key
            self.__query_set_text_key.value = value
            self.__query_set_text_key.execute()

        elif key in real_keys:
            self.__query_set_real_key.id = id
            self.__query_set_real_key.key = key
            self.__query_set_real_key.value = value
            self.__query_set_real_key.execute()

        elif key == "stock":
            self.__query_set_int_key.id = id
            self.__query_set_int_key.key = key
            self.__query_set_int_key.value = value
            self.__query_set_int_key.execute()
        else:
            raise KeyError

    def add(self, category, name, details, guaranty, cost, price, stock):
        self.__query_add.id = int(self.settings.get("last_product_id")) + 1
        self.__query_add.category = category
        self.__query_add.name = name
        self.__query_add.details = details
        self.__query_add.guaranty = guaranty
        self.__query_add.cost = cost
        self.__query_add.price = price
        self.__query_add.stock = stock
        self.__query_add.execute()
        self.settings.set("last_product_id", self.__query_add.id)
        return self.__query_add.id

    def delete(self, id):
        self.__query_delete.id = id
        self.__query_delete.execute()


class Sellers(object):
    def __init__(self, branch):
        self.branch = branch
        self.settings = Settings(self.branch)
        self.__query_insert = catrina.Query(
            "INSERT INTO sellers VALUES ({{id:integer}}, 1, '{{name:text}}',{{commission:real}});", self.branch)
        self.__query_select_actives = catrina.Query("SELECT id, name, commission FROM sellers WHERE active=1;",
                                                    self.branch, ("id", "name", "commission"))
        self.__query_delete = catrina.Query("UPDATE sellers SET active=0 WHERE id={{id:integer}};", self.branch)
        self.__query_edit_name = catrina.Query("UPDATE sellers SET name='{{name:text}}' WHERE id={{id:integer}};",
                                               self.branch)
        self.__query_edit_commission = catrina.Query(
            "UPDATE sellers SET commission={{commission:real}} WHERE id={{id:integer}};", self.branch)

        self.__query_get = catrina.Query("SELECT id, name, commission FROM sellers WHERE id={{id:integer}};",
                                         self.branch,
                                         ("id", "name", "commission"))

    def get(self, id):
        self.__query_get.id = id
        seller = self.__query_get.execute()

        if len(seller) > 0:
            return seller[0]
        else:
            return None

    def get_all(self):
        pass

    def get_actives(self):
        return self.__query_select_actives.execute()

    def add(self, name, commission):
        self.__query_insert.id = int(self.settings.get("last_seller_id")) + 1
        self.__query_insert.name = name
        self.__query_insert.commission = commission
        self.__query_insert.execute()
        self.settings.set("last_seller_id", str(self.__query_insert.id))
        return self.__query_insert.id

    def edit_name(self, id, name):
        self.__query_edit_name.id = id
        self.__query_edit_name.name = name
        self.__query_edit_name.execute()

    def edit_commission(self, id, commission):
        self.__query_edit_commission.id = id
        self.__query_edit_commission.commission = float(commission)
        self.__query_edit_commission.execute()

    def delete(self, id):
        self.__query_delete.id = id
        self.__query_delete.execute()
