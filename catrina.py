# -*- coding: utf-8 -*-

import collections

import os

import re

import random

import sqlite3

ESCAPE_CHARACTERS = {';': '__catrina_escape_semicolon__',
                     '\"': '__catrina_escape_doublequotation__',
                     '\'': '__catrina_escape_singlequotation__',
                     '*': '__catrina_escape_asterisk__',
                     '--': '__catrina_escape_doubledash__',
                     }


def create(path):

    while True:
        if os.path.exists(path):
            path = "{0}.duplicate".format(path)
        else:
            break

    conn = sqlite3.connect(path)
    conn.close()

    return  path


class TextTemplate(object):

    def __init__(self, template):
        """
        Usage:

        template = TextTemplate("Text: {{text}}")
        template.text = "STRING"



        template_with_type = TextTemplate("Number: {{num:mytype}}")

        def process_mytype(the_type, the_value):
            processed_value = int(value)
            return processed_value

        template_with_type.num = 5, process_mytype


        :param template: The string that will be formatted
        """

        self.__variables = {}

        try:
            with open(template) as file:
                self.__template = file.read()
        except:
            self.__template = template

    def compile(self):

        final = self.__template

        for k in self.__variables.keys():
            varname = "{{" + k + ":" + self.__variables[k] + "}}" if self.__variables[
                                                                         k] is not None else "{{" + k + "}}"
            final = final.replace(varname, str(self.__dict__[k]))

        return final

    def keys(self):
        return self.__variables.keys()

    def types(self):
        return self.__variables.copy()

    def __process_template(self, template):
        regex = r"\{\{(\s*[a-zA-Z]\w*\s*)(:){0,1}((\s*[a-zA-Z]\w*\s*)){0,1}\}\}"
        matches = re.finditer(regex, template, re.MULTILINE)

        for match in enumerate(matches):
            var = match[1].group().replace(" ", "").replace("{{", "").replace("}}", "").split(":")
            self.__dict__[var[0]] = None
            if len(var) > 1:
                self.__variables[var[0]] = var[1]
            else:
                self.__variables[var[0]] = None
            template = template.replace(match[1].group(), match[1].group().replace(" ", ""))

        return template

    def __setattr__(self, key, value):

        if key == "_TextTemplate__variables":
            self.__dict__[key] = value
        elif key == "_TextTemplate__template":
            self.__dict__[key] = self.__process_template(value)
        else:
            if key in self.__variables.keys():
                if self.__variables[key] is None:
                    self.__dict__[key] = value
                else:
                    try:
                        self.__dict__[key] = value[1](self.__variables[key], value[0])
                    except TypeError:
                        raise NotImplementedError("Validation function not defined")

            else:
                raise KeyError

    def __setitem__(self, key, value):
        if key in self.__variables.keys():
            if self.__variables[key] is None:
                self.__dict__[key] = value
            else:
                try:
                    self.__dict__[key] = value[1](value[0])
                except TypeError:
                    raise NotImplementedError("Validation function not defined")

        else:
            raise KeyError

    def __getitem__(self, item):
        if item in self.__variables.keys():
            return self.__dict__[item]
        else:
            raise KeyError

    def __str__(self):
        return self.__template


class Query(TextTemplate):

    def __init__(self, statement, database, fields=None):

        if not os.path.exists(database):
            raise sqlite3.DatabaseError

        if statement.count(';') == 1:
            if statement.strip()[-1] == ';':
                TextTemplate.__init__(self, statement)
            else:
                raise ValueError
        else:
            raise ValueError

        self.__dict__['database'] = database
        self.__dict__['fields'] = fields

        if self.fields is not None:
            self.__dict__['_Query__row'] = collections.namedtuple('Row', fields)

    def __validate_value(self, type, value):
        if type == 'integer':
            return int(float(value))
        elif type == 'real':
            return float(value)
        elif type == 'text':
            code = ESCAPE_CHARACTERS
            if isinstance(value, tuple):
                code = value[1]
                value = value[0]
            text = str(value)
            for c in code.keys():
                text = text.replace(c, code[c])
            return text
        elif type == 'blob':
            return value
        else:
            return value[1](type, value[0])

    def execute(self, decode=True, code=None, fetch_one=False):
        if self.compile().count(';') > 1:
            raise ValueError

        if code is None:
            code = ESCAPE_CHARACTERS
        else:
            decode = True

        conn = sqlite3.connect(self.database)
        cursor = conn.cursor()

        if fetch_one:
            rows = cursor.execute(self.compile()).fetchone()
        else:
            rows = cursor.execute(self.compile()).fetchall()

        conn.commit()
        conn.close()

        if rows is None or len(rows) == 0:
            return []

        final_rows = []

        if decode:

            str_index = []
            sample = rows[0]
            for i in range(len(sample)):
                if isinstance(sample[i], str):
                    str_index.append(i)

            for row in rows:
                r = list(row)
                for index in str_index:
                    for c in code:
                        r[index] = r[index].replace(code[c], c)
                final_rows.append(r)

        if self.fields is not None:
            if fetch_one:
                rows = self.__row(rows)
            else:
                rows = list(map(self.__row._make, final_rows))
        elif decode:
            for i in range(len(final_rows)):
                final_rows[i] = tuple(final_rows[i])
            rows = final_rows

        return rows

    def __setattr__(self, key, value):
        if key is '_TextTemplate__variables' or key is '_TextTemplate__template':
            super().__setattr__(key, value)
        elif self.types()[key] is not None:
            super().__setattr__(key, (value, self.__validate_value))
        else:
            super().__setattr__(key, value)

    def __setitem__(self, key, value):
        if self.types()[key] is not None:
            super().__setattr__(key, (value, self.__validate_value))
        else:
            super().__setattr__(key, value)


class Script(TextTemplate):

    def __init__(self, statement, database):

        TextTemplate.__init__(self, statement)

        if not os.path.exists(database):
            raise sqlite3.DatabaseError

        self.__dict__['database'] = database

    def __validate_value(self, type, value):
        if type == 'integer':
            return int(float(value))
        elif type == 'real':
            return float(value)
        elif type == 'text':
            code = ESCAPE_CHARACTERS
            if isinstance(value, tuple):
                code = value[1]
                value = value[0]
            text = str(value)
            for c in code.keys():
                text = text.replace(c, code[c])
            return text
        elif type == 'blob':
            return value
        else:
            return value[1](type, value[0])

    def execute(self):

        conn = sqlite3.connect(self.database)
        cursor = conn.cursor()

        rows = cursor.executescript(self.compile())

        conn.commit()
        conn.close()

    def __setattr__(self, key, value):
        if key is '_TextTemplate__variables' or key is '_TextTemplate__template':
            super().__setattr__(key, value)
        elif self.types()[key] is not None:
            super().__setattr__(key, (value, self.__validate_value))
        else:
            super().__setattr__(key, value)

    def __setitem__(self, key, value):
        if self.types()[key] is not None:
            super().__setattr__(key, (value, self.__validate_value))
        else:
            super().__setattr__(key, value)
