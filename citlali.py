# -*- coding: utf-8 -*

__version__ = 1.0
__author__ = "Gato Wormius - gatowormius@gmail.com"

import gi
gi.require_version("Gtk","3.0")

from gi.repository import Gtk

import models

import controllers

if __name__ == '__main__':

    models.check_init()

    branch = controllers.Splash().run()

    if branch is not None:

        controllers.Main(branch).run()



